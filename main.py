import random
import numpy as np
import cv2

img = cv2.imread(str('image.jpg'), 0)
shape = img.shape
rows = int(shape[0])
columns = int(shape[1])

def imageToBits():
   cv2.imshow('image', img)
   cv2.waitKey(0)
   cv2.destroyAllWindows()
   np.set_printoptions(threshold=np.inf)
   np.core.arrayprint._line_width = 100
   bitArray = np.zeros(shape=(rows, columns))
   for i in range(0, rows):
       for j in range(0, columns):
           if img[i][j] < 127:
               img[i][j] = 0
               bitArray[i][j] = 255
           elif img[i][j] >= 127:
               img[i][j] = 255
               bitArray[i][j] = 0
   return bitArray

def imageBitsToFile(bitArray, name):
   with open(str(name), 'w') as f:
       for i in range(0, rows):
           list = bitArray[i, :].tolist()
           for item in list:
               f.write("%s " % int(item))
           f.write('\n')


def generateShares(bitArray):
   firstShare = np.zeros([2 * rows, 2 * columns], dtype=np.uint8)
   secondShare = np.zeros([2 * rows, 2 * columns], dtype=np.uint8)
   # list1 = [0,0,255,255]
   # list2 = [255,255,0,0]
   # list3 = [0,255,0,255]
   # list4 = [255,0,255,0]
   # list5 = [0,255,255,0]
   # list6 = [255,0,0,255]
   k = 0
   l = 0
   for i in range(0, rows):
       for j in range(0, columns):
           if bitArray[i][j] == 0:
               randomNumber = random.randrange(0, 5)
               if randomNumber == 0:
                   firstShare[l][k] = 0
                   secondShare[l][k] = 0
                   firstShare[l][k + 1] = 0
                   secondShare[l][k + 1] = 0
                   firstShare[l + 1][k] = 255
                   secondShare[l + 1][k] = 255
                   firstShare[l + 1][k + 1] = 255
                   secondShare[l + 1][k + 1] = 255

               elif randomNumber == 1:
                   firstShare[l][k] = 255
                   secondShare[l][k] = 255
                   firstShare[l][k + 1] = 255
                   secondShare[l][k + 1] = 255
                   firstShare[l + 1][k] = 0
                   secondShare[l + 1][k] = 0
                   firstShare[l + 1][k + 1] = 0
                   secondShare[l + 1][k + 1] = 0

               elif randomNumber == 2:
                   firstShare[l][k] = 0
                   secondShare[l][k] = 0
                   firstShare[l][k + 1] = 255
                   secondShare[l][k + 1] = 255
                   firstShare[l + 1][k] = 0
                   secondShare[l + 1][k] = 0
                   firstShare[l + 1][k + 1] = 255
                   secondShare[l + 1][k + 1] = 255

               elif randomNumber == 3:
                   firstShare[l][k] = 255
                   secondShare[l][k] = 255
                   firstShare[l][k + 1] = 0
                   secondShare[l][k + 1] = 0
                   firstShare[l + 1][k] = 255
                   secondShare[l + 1][k] = 255
                   firstShare[l + 1][k + 1] = 0
                   secondShare[l + 1][k + 1] = 0

               elif randomNumber == 4:
                   firstShare[l][k] = 0
                   secondShare[l][k] = 0
                   firstShare[l][k + 1] = 255
                   secondShare[l][k + 1] = 255
                   firstShare[l + 1][k] = 255
                   secondShare[l + 1][k] = 255
                   firstShare[l + 1][k + 1] = 0
                   secondShare[l + 1][k + 1] = 0

               elif randomNumber == 5:
                   firstShare[l][k] = 255
                   secondShare[l][k] = 255
                   firstShare[l][k + 1] = 0
                   secondShare[l][k + 1] = 0
                   firstShare[l + 1][k] = 0
                   secondShare[l + 1][k] = 0
                   firstShare[l + 1][k + 1] = 255
                   secondShare[l + 1][k + 1] = 255
               k = k + 2
           elif bitArray[i][j] == 255:
               randomNumber = random.randrange(0, 5)
               if randomNumber == 0:
                   firstShare[l][k] = 0
                   secondShare[l][k] = 255
                   firstShare[l][k + 1] = 0
                   secondShare[l][k + 1] = 255
                   firstShare[l + 1][k] = 255
                   secondShare[l + 1][k] = 0
                   firstShare[l + 1][k + 1] = 255
                   secondShare[l + 1][k + 1] = 0

               elif randomNumber == 1:
                   firstShare[l][k] = 255
                   secondShare[l][k] = 0
                   firstShare[l][k + 1] = 255
                   secondShare[l][k + 1] = 0
                   firstShare[l + 1][k] = 0
                   secondShare[l + 1][k] = 255
                   firstShare[l + 1][k + 1] = 0
                   secondShare[l + 1][k + 1] = 255

               elif randomNumber == 2:
                   firstShare[l][k] = 0
                   secondShare[l][k] = 255
                   firstShare[l][k + 1] = 255
                   secondShare[l][k + 1] = 0
                   firstShare[l + 1][k] = 0
                   secondShare[l + 1][k] = 255
                   firstShare[l + 1][k + 1] = 255
                   secondShare[l + 1][k + 1] = 0

               elif randomNumber == 3:
                   firstShare[l][k] = 255
                   secondShare[l][k] = 0
                   firstShare[l][k + 1] = 0
                   secondShare[l][k + 1] = 255
                   firstShare[l + 1][k] = 255
                   secondShare[l + 1][k] = 0
                   firstShare[l + 1][k + 1] = 0
                   secondShare[l + 1][k + 1] = 255

               elif randomNumber == 4:
                   firstShare[l][k] = 0
                   secondShare[l][k] = 255
                   firstShare[l][k + 1] = 255
                   secondShare[l][k + 1] = 0
                   firstShare[l + 1][k] = 255
                   secondShare[l + 1][k] = 0
                   firstShare[l + 1][k + 1] = 0
                   secondShare[l + 1][k + 1] = 255

               elif randomNumber == 5:
                   firstShare[l][k] = 255
                   secondShare[l][k] = 0
                   firstShare[l][k + 1] = 0
                   secondShare[l][k + 1] = 255
                   firstShare[l + 1][k] = 0
                   secondShare[l + 1][k] = 255
                   firstShare[l + 1][k + 1] = 255
                   secondShare[l + 1][k + 1] = 0
               k = k + 2
           if k >= 2 * rows:
               k = 0
       l = l + 2
   arrays = [firstShare, secondShare]
   return arrays


def merge(arrays):
   array = np.zeros(shape=(rows, columns))
   share1 = arrays[0]
   share2 = arrays[1]
   y = 0
   for i in range(0, len(share1), 2):
       x = 0
       for j in range(0, len(share1[0]), 2):
           if share1[i][j] == share2[i][j] and share1[i][j + 1] == share2[i][j + 1] and share1[i + 1][j] == \
                   share2[i + 1][j] and share1[i + 1][j + 1] == share2[i + 1][j + 1]:
               array[y][x] = 0
               x = x + 1
           else:
               array[y][x] = 255
               x = x + 1
       y = y + 1
   return array


def main():
   imageBitsToFile(imageToBits(), 'file.txt')
   arrays = generateShares(imageToBits())
   imageBitsToFile(arrays[0], 'share1.txt')
   imageBitsToFile(arrays[1], 'share2.txt')
   cv2.imwrite('image_share1.jpg', arrays[0])
   cv2.imwrite('image_share2.jpg', arrays[1])
   array = merge(arrays)
   cv2.imwrite('result.jpg', array)


main()


